import { useState } from "react";
import AccountLogIn from "./AccountLogin";
import "./AccountLoginModal.css";
import PropTypes from 'prop-types';
import { AccountLoginModalContext } from "./AccountLoginModalContext"


export function AccountLoginModal({ isAccountLoginModalOpen, closeAccountLoginModal }) {
  return (
    <div className={isAccountLoginModalOpen ? "signInModel show" : "signInModel"}>
      <div>
        <AccountLogIn closeAccountLoginModal={closeAccountLoginModal} />
      </div>
    </div>
  );
}


AccountLoginModal.propTypes = {
  isAccountLoginModalOpen: PropTypes.bool.isRequired,
  closeAccountLoginModal: PropTypes.func.isRequired,
};


export function AccountLoginModalProvider({ children }) {
  const [isAccountLoginModalOpen, setIsAccountLoginModalOpen] = useState(false);

  const openAccountLoginModal = () => setIsAccountLoginModalOpen(true);
  const closeAccountLoginModal = () => setIsAccountLoginModalOpen(false);

  return (
    <AccountLoginModalContext.Provider value={{ isAccountLoginModalOpen, openAccountLoginModal, closeAccountLoginModal }}>
      {children}
      <AccountLoginModal isAccountLoginModalOpen={isAccountLoginModalOpen} closeAccountLoginModal={closeAccountLoginModal} />
    </AccountLoginModalContext.Provider>
  );
}


AccountLoginModalProvider.propTypes = {
  children: PropTypes.node.isRequired,
};
