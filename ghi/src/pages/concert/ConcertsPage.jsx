import { useState, useEffect } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import "./ConcertsPage.css";


const ConcertsPage = () => {
    const [concerts, setConcerts] = useState([]);
    const [artist, setArtist] = useState("");

    useEffect(() => {
        const fetchData = async () => {
            try {
                const response = await fetch('https://concerts-artists-events-tracker.p.rapidapi.com/artist?name=Green%20Day&page=1', {
                    method: 'GET',
                    headers: {
                        'X-RapidAPI-Host': 'concerts-artists-events-tracker.p.rapidapi.com'
                    }
                });

                if (!response.ok) {
                    throw new Error(`HTTP error! status: ${response.status}`);
                }

                const data = await response.json();
                const usConcerts = data.data.filter(concert => concert.location.address.addressCountry === 'US').slice(0, 10);
                setConcerts(usConcerts);
                setArtist(data.data[1].name);

            } catch (error) {
                console.error('Error fetching data: ', error);
            }
        };

        fetchData();
    }, []);

    return (
        <div className="container mt-4" style={{ backgroundColor: 'black', color: 'pink' }}>
            <h1 className="typing-demo "> {artist} Upcoming Concerts in the US</h1>
            <ul className="list-group list-group-flush">
                {concerts.map((concert, index) => (
                    <li key={index} className="list-group-item bg-black border-dark text-white">
                        <div className="d-flex justify-content-between align-items-center">
                            <div>
                                <h5 className="mb-0">{new Date(concert.startDate).toLocaleDateString('en-US', { year: 'numeric', month: 'long', day: 'numeric' })}</h5>
                                <p className="mb-0">{concert.location.name}</p>
                                <p className="mb-0">{concert.location.address.addressLocality}, {concert.location.address.addressCountry}</p>
                            </div>
                            <a href={`https://www.ticketmaster.com/search?q=${concert.name}`} className="btn btn-red">Tickets</a>
                        </div>
                    </li>
                ))}
            </ul>
        </div>
    );
};

export default ConcertsPage;
