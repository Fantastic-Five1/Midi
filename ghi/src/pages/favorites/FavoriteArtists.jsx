import "./Favorites.css";
import Loading from "../../extrafunctions/helpers/Loading";
import { useState, useEffect } from "react";
import { useToast } from "../../context/toastcontext/useToast";
import { useNavigate } from "react-router-dom";
import { useData } from "../../context/datacontext/useData";
import { NavLink } from "react-router-dom";



export default function FavoriteArtists() {
    const [favoriteArtists, setFavoriteArtist] = useState([]);
    const [isLoading, setIsLoading] = useState(true);
    const [loaded, setLoaded] = useState(true);
    const { setReleaseMbid, setArtistMbid, setArtistName, setReleaseGroupMbid, setAlbumName } = useData();
    const showToast = useToast();
    const navigate = useNavigate();


    useEffect(() => {
        setReleaseMbid("")
        setReleaseGroupMbid("")
        setAlbumName("")
        setArtistName("")
        setArtistMbid("")
        const getData = async () => {
            const response = await fetch(
                `${import.meta.env.VITE_APP_API_HOST}/api/user/favorite_artist`, {
                credentials: 'include',
                headers: {
                    "Content-Type": "application/json",
                },
            }
            );
            if (response.ok) {
                const data = await response.json();
                setFavoriteArtist(data)
            }
        };
        setLoaded(false)
        setIsLoading(false)
        getData()
    }, [loaded, setAlbumName, setArtistMbid, setArtistName, setReleaseGroupMbid,
        setReleaseMbid]);



    async function handleDeleteClick(id) {
        const fetchOption = {
            method: "DELETE",
            credentials: 'include',
            headers: {
                "Content-Type": "application/json",
            },
        };

        const request = await fetch(
            `${import.meta.env.VITE_APP_API_HOST}/api/favorite_artist/${id}/delete`,
            fetchOption
        );

        if (request.ok) {
            showToast("Removed From Favorites!", "error");
            setLoaded(true)
        } else {
            showToast("Error Adding To Favorites!", "error");
        }
    }



    const handleDetailClick = (e) => {
        localStorage.removeItem('artistMbid');
        localStorage.removeItem('artistName');
        const artistDiv = e.currentTarget.closest("#artistInfo");
        const name = artistDiv.getAttribute("data-name");
        setArtistName(name);
        const artistMbid = artistDiv.getAttribute("data-mbid");
        setArtistMbid(artistMbid);
        navigate("/artist");
    };

    if (isLoading) {
        return (
            <div style={{ display: "flex", justifyContent: "center" }}>
                <Loading />
            </div>
        );
    }



    const favoritesImageDivs = document.querySelectorAll('.favoritesImageDiv');

    favoritesImageDivs.forEach(div => {
        div.addEventListener('mouseenter', () => {
            favoritesImageDivs.forEach(otherDiv => otherDiv.classList.remove('active'));
            div.classList.add('active');
        });
    });


    return (
        <div className="mainFavoritesDiv">
            <div className="favoritesNameDiv">
                <h1 className="favoritesName">Favorite Artists</h1>
            </div>
            <div className={favoriteArtists.length < 1 ? "mainFavoritesDivChildSmall" : "mainFavoritesDivChild"}>
                {favoriteArtists.length < 1 && (
                    <div className="noArtistsAdded">
                        <h1>
                            No Artists Added
                        </h1>
                    </div>
                )}
                {favoriteArtists.map((favoriteArtist) => {
                    return (
                        <div
                            key={favoriteArtist.id}
                            data-name={favoriteArtist.name}
                            data-mbid={favoriteArtist.artist_mbid}
                            id="artistInfo"
                            className="FavoritesGridsDiv"
                        >
                            <div className="FavoritesGridsDivChild">
                                <div className="FavoritesNameAndButtonDiv">
                                    <div className="FavoritesNameDiv">
                                        <h1 className="FavoritesName">
                                            <NavLink
                                                to="/artist"
                                                onClick={handleDetailClick}
                                                className="FavoritesName"
                                            >
                                                {favoriteArtist.name}
                                            </NavLink>
                                        </h1>
                                    </div>
                                    <div className="FavoritesButtonDiv">
                                        <button className="FavoritesButton"
                                            onClick={() => handleDeleteClick(favoriteArtist.id)}>
                                            -
                                        </button>
                                    </div>
                                </div>
                                <div className="favoritesImageDiv">
                                    <div className="photoDiv">
                                        <div className="overlayPhoto"></div>
                                        <img
                                            onClick={handleDetailClick}
                                            className="FavoritesImage"
                                            src={`https://coverartarchive.org/release-group/${favoriteArtist.release_group_mbid}/front`}
                                        />
                                        <img src="/cdArt2.png" alt="CD" className="cd" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    );
                })}
            </div>
        </div>
    );
}
