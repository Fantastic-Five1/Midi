import "./Favorites.css";
import Loading from "../../extrafunctions/helpers/Loading";
import { useState, useEffect } from "react";
import { useToast } from "../../context/toastcontext/useToast";
import { NavLink } from "react-router-dom";
import { useData } from "../../context/datacontext/useData";
import { useNavigate } from "react-router-dom";


export default function FavoriteAlbums() {
    const [favoriteAlbums, setFavoriteAlbums] = useState([]);
    const [isLoading, setIsLoading] = useState(true);
    const [loaded, setLoaded] = useState(true);
    const { setReleaseMbid, setArtistMbid, setArtistName, setReleaseGroupMbid, setAlbumName } = useData();
    const navigate = useNavigate()
    const showToast = useToast();


    useEffect(() => {
        setReleaseMbid("")
        setReleaseGroupMbid("")
        setAlbumName("")
        setArtistName("")
        setArtistMbid("")
        const getData = async () => {
            const response = await fetch(
                `${import.meta.env.VITE_APP_API_HOST}/api/user/favorite_album`,
                {
                    credentials: 'include',
                    headers: {
                        "Content-Type": "application/json",
                    },
                }
            );
            if (response.ok) {
                const data = await response.json();
                setFavoriteAlbums(data);
            }
        };
        getData();
        setLoaded(false)
        setIsLoading(false)
    }, [loaded, setAlbumName, setArtistMbid, setArtistName, setReleaseGroupMbid,
        setReleaseMbid]);


    async function handleDeleteClick(id) {
        const fetchOption = {
            method: "DELETE",
            credentials: 'include',
            headers: {
                "Content-Type": "application/json",
            },
        };

        const request = await fetch(
            `${import.meta.env.VITE_APP_API_HOST}/api/favorite_album/${id}/delete`,
            fetchOption
        );

        if (request.ok) {
            showToast("Removed From Favorites!", "error");
            setLoaded(true)
        } else {
            showToast("Error", "error");
        }
    }

    const handleDetailClick = (e) => {
        localStorage.removeItem("releaseGroupMbid");
        localStorage.removeItem("albumName");
        const albumDiv = e.currentTarget.closest("#albumInfo");
        const artistName = albumDiv.getAttribute("data-artist-name");
        const releaseGroupMbid = albumDiv.getAttribute("data-releasegroupmbid");
        const albumName = albumDiv.getAttribute("data-album-name");
        const artistMbid = albumDiv.getAttribute("data-artist-mbid");
        setArtistName(artistName);
        setArtistMbid(artistMbid);
        setReleaseGroupMbid(releaseGroupMbid);
        setAlbumName(albumName);
        navigate("/album");
    };

    if (isLoading) {
        return (
            <div style={{ display: "flex", justifyContent: "center" }}>
                <Loading />
            </div>
        );
    }


    const favoritesImageDivs = document.querySelectorAll('.favoritesImageDiv');

    favoritesImageDivs.forEach(div => {
        div.addEventListener('mouseenter', () => {
            favoritesImageDivs.forEach(otherDiv => otherDiv.classList.remove('active'));
            div.classList.add('active');
        });
    });



    return (
        <div className="mainFavoritesDiv">
            <div className="favoritesNameDiv">
                <h1 className="favoritesName">Favorite Albums</h1>
            </div>
            <div className="mainFavoritesDivChild">
                {favoriteAlbums.map((favoriteAlbum) => {
                    return (
                        <div
                            id="albumInfo"
                            key={favoriteAlbum.id}
                            data-artist-name={favoriteAlbum.artist_name}
                            data-releasegroupmbid={favoriteAlbum.release_group_mbid}
                            data-album-name={favoriteAlbum.name}
                            data-artist-mbid={favoriteAlbum.artist_mbid}

                            className="FavoritesGridsDiv"
                        >
                            <div className="FavoritesGridsDivChild">
                                <div className="FavoritesNameAndButtonDiv">
                                    <div className="FavoritesNameDiv">
                                        <NavLink className="FavoritesName" to="/album"
                                            onClick={handleDetailClick}>
                                            {favoriteAlbum.name.length > 25
                                                ? `${favoriteAlbum.name.substring(0, 25)}...`
                                                : favoriteAlbum.name}
                                        </NavLink>
                                    </div>
                                    <div className="FavoritesButtonDiv">
                                        <button
                                            onClick={() => handleDeleteClick(favoriteAlbum.id)}
                                            className="FavoritesButton"
                                        >
                                            -
                                        </button>
                                    </div>
                                </div>
                                <div className="favoritesImageDiv">
                                    <div className="photoDiv">
                                        <div className="overlayPhoto"></div>
                                        <img
                                            onClick={handleDetailClick}
                                            className="FavoritesImage"
                                            src={`https://coverartarchive.org/release-group/${favoriteAlbum.release_group_mbid}/front`}
                                        />
                                        <img src="/cdArt3.png" alt="CD" className="cd" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    );
                })}
            </div>
        </div>
    );
}
