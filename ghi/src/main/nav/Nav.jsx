import "./Nav.css";
import { NavLink } from "react-router-dom";
import { useState, useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
    faMagnifyingGlass,
    faBars,
    faX,
} from "@fortawesome/free-solid-svg-icons";
import useWindowSize from "../../extrafunctions/helpers/WindowSize";
import { useNavigate } from "react-router-dom";
import { useData } from "../../context/datacontext/useData";
import { useAccountLoginModalContext } from "../../modals/accounts/login/useAccountLoginModalContext";
import { useAuthContext } from "../../authorization/Authorization";
import useToken from "../../authorization/Authorization";
import { useToast } from "../../context/toastcontext/useToast";


function Nav() {
    const { width } = useWindowSize();
    const [menuDisplay, setMenuDisplay] = useState(false);
    const [searchInputChange, setSearchInputChange] = useState("");
    const [searchIsBuffering, setSearchIsBuffering] = useState(false);
    const { openAccountLoginModal } = useAccountLoginModalContext();
    const { setSearchInput, setReleaseMbid, setArtistMbid, setArtistName, setReleaseGroupMbid, setAlbumName } = useData();
    const navigate = useNavigate();
    const { logout } = useToken();
    const { token } = useAuthContext();
    const showToast = useToast();
    const menuClassName = menuDisplay ? "mainMenuShow" : "mainMenu";

    const handleResize = () => {
        if (window.innerWidth >= 900) {
            setMenuDisplay(false);
        }
    };


    useEffect(() => {
        window.addEventListener('resize', handleResize);
        return () => {
            window.removeEventListener('resize', handleResize);
        };
    }, []);

    const closeMenu = () => {
        setMenuDisplay(false);
    };

    const handleInputChange = (e) => {
        setSearchInputChange(e.target.value);
    };

    const artistLinkClicked = () => {
        if (!token) {
            showToast("Please Login!", "error")
            openAccountLoginModal()
        } else {
            navigate("/favorite/artists")
        }
    }

    const albumLinkClicked = () => {
        if (!token) {
            showToast("Please Login!", "error")
            openAccountLoginModal()
        } else {
            navigate("/favorite/albums")
        }
    }

    function startTimer() {
        let counter = 600
        setSearchIsBuffering(true)
        const intervalId = setInterval(() => {
            counter--;
            if (counter === 0) {
                clearInterval(intervalId);
                setSearchIsBuffering(false)
                counter = 600
            }
        }, 1);
    }

    const handleAction = () => {
        if (searchIsBuffering === false) {
            startTimer();
            setSearchInput(searchInputChange);
            navigate("/search");
            setReleaseMbid("")
            setReleaseGroupMbid("")
            setAlbumName("")
            setArtistName("")
            setArtistMbid("")
            setSearchInputChange("");
        } else {
            showToast("Please Wait!", "error")
        }
    };

    const handleKeyDown = (e) => {
        if (e.key === "Enter") {
            handleAction();
        }
    };

    return (
        <nav>
            <div className="logo">
                <NavLink to="/" className="linkHome">
                    MIDI
                </NavLink>
                <img
                    onClick={() => navigate("/")}
                    className="musicIconNav"
                    src="/musicIcon.svg"
                />
            </div>
            <div className="openMenu" onClick={() => setMenuDisplay(true)}>
                <FontAwesomeIcon className="iconMenu" icon={faBars} />
            </div>
            {width <= 900 &&(
                <ul
                    className={menuClassName}
                >
                    <li className="li">
                        <button onClick={() => {
                            artistLinkClicked()
                            closeMenu()
                        }}
                            className="link"
                            to="/favorite/artists"
                        >
                            My Artists
                        </button>
                    </li>
                    <li className="li">
                        <button onClick={() => {
                            albumLinkClicked()
                            closeMenu()
                        }}
                            className="link"
                            to="/favorite/albums"
                        >
                            My Albums
                        </button>
                    </li>
                    <div className="closeMenu" onClick={closeMenu}>
                        <FontAwesomeIcon className="iconX" icon={faX} />
                    </div>
                </ul>
            )}
            {width > 900 && (
                    <ul
                    className={"above900UL"}
                    >
                        <li className="li">
                            <button onClick={() => {
                                artistLinkClicked()
                                closeMenu()
                            }}
                                className="link"
                                to="/favorite/artists"
                            >
                                My Artists
                            </button>
                        </li>
                        <li className="li">
                            <button onClick={() => {
                                albumLinkClicked()
                                closeMenu()
                            }}
                                className="link"
                                to="/favorite/albums"
                            >
                                My Albums
                            </button>
                        </li>
                        <div className="closeMenu" onClick={closeMenu}>
                            <FontAwesomeIcon className="iconX" icon={faX} />
                        </div>
                    </ul>
                )}
            <div className="searchAndSignInDiv">
                <div className="searchInputDiv">
                    <input
                        className="searchInput"
                        placeholder={width < 450 ? "Search" : "Search Artists"}
                        type="text"
                        value={searchInputChange}
                        onChange={handleInputChange}
                        onKeyDown={handleKeyDown}
                    />
                    <FontAwesomeIcon
                        icon={faMagnifyingGlass}
                        onClick={handleAction}
                        className="searchIcon"
                    />
                </div>
                <div>
                    {!token && (
                        <button className="signIn" onClick={openAccountLoginModal}>
                            Sign In
                        </button>
                    )}
                </div>
                <div>
                    {token && (
                        <button className="signIn" onClick={() => {
                            logout()
                            showToast(`See You Soon!`, "success")
                        }}
                        >
                            Sign Out
                        </button>
                    )}
                </div>
            </div>
        </nav>
    );
}

export default Nav;
