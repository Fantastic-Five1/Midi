from pydantic import BaseModel
from typing import List, Union
from queries.pool import pool
from fastapi import (
    HTTPException,
    status,
)


class Error(BaseModel):
    message: str


class FavoriteArtistIn(BaseModel):
    name: str
    artist_mbid: str
    release_group_mbid: str
    account_id: int


class FavoriteArtistOut(BaseModel):
    id: int
    name: str
    artist_mbid: str
    release_group_mbid: str
    account_id: int


class WikipediaSummaryResponse(BaseModel):
    summary: str


class FavoriteArtistRepository:
    def get_all(
        self, account_id: int
    ) -> Union[List[FavoriteArtistOut], Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id
                            , name
                            , artist_mbid
                            , release_group_mbid
                            , account_id
                        FROM favorite_artist
                        WHERE account_id = %s
                        """,
                        [account_id],
                    )
                    result = []
                    for record in db:
                        favorite_artist = FavoriteArtistOut(
                            id=record[0],
                            name=record[1],
                            artist_mbid=record[2],
                            release_group_mbid=record[3],
                            account_id=record[4],
                        )
                        result.append(favorite_artist)
                    return result

        except Exception as e:
            print(e)
            return {"message": "Could not get all favorite_artist"}

    def favorite_artist_exists(self, artist_mbid, account_id) -> bool:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    SELECT EXISTS (
                        SELECT 1 FROM favorite_artist
                        WHERE artist_mbid = %s AND account_id = %s
                    );
                    """,
                    [artist_mbid, account_id],
                )
                return db.fetchone()[0]

    def create(self, favorite_artist: FavoriteArtistIn) -> FavoriteArtistOut:
        if self.favorite_artist_exists(
            favorite_artist.artist_mbid, favorite_artist.account_id
        ):
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="User has already favorited this Artist",
            )
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO favorite_artist
                            (name, artist_mbid, release_group_mbid, account_id)
                        VALUES
                            (%s, %s, %s, %s)
                        RETURNING id;
                        """,
                        [
                            favorite_artist.name,
                            favorite_artist.artist_mbid,
                            favorite_artist.release_group_mbid,
                            favorite_artist.account_id,
                        ],
                    )
                    id = result.fetchone()[0]
                    return self.favorite_artist_in_to_out(id, favorite_artist)

        except Exception as e:
            print(e)
            return {"message": "Could not create favorite_artist"}

    def delete(self, favorite_artist_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM favorite_artist
                        WHERE id = %s
                        """,
                        [favorite_artist_id],
                    )
                    return True

        except Exception as e:
            print(e)
            return False

    def favorite_artist_in_to_out(
        self, id: int, favorite_artist: FavoriteArtistIn
    ):
        old_data = favorite_artist.dict()
        return FavoriteArtistOut(id=id, **old_data)
