steps = [
    [
        # "Up" SQL statement
        """
            CREATE TABLE IF NOT EXISTS review (
                id SERIAL PRIMARY KEY,
                rating DECIMAL(3, 1) CHECK (rating >= 0.5 AND rating <= 5.0)
                    NOT NULL,
                comment TEXT,
                created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
                release_group_mbid VARCHAR(100) NOT NULL,
                account_username VARCHAR REFERENCES account(username) ON
                    DELETE CASCADE NOT NULL
            );
        """,
        # "Down" SQL statement
        """
            DROP TABLE review;
        """
    ]
]
