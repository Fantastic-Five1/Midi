from fastapi.testclient import TestClient
from authenticator import authenticator
from main import app
from queries.review import ReviewRepository


client = TestClient(app)


class TestReviewRepository:
    def get_all(
        self,
        account_id
    ):
        return []


def fake_account():
    return {"id": "1", "username": "user"}


def test_get_all_reviews():
    # Arrange
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_account

    app.dependency_overrides[
        ReviewRepository
    ] = TestReviewRepository

    # Act
    response = client.get("api/{release_group_mbid}/review")

    # Clean Up
    app.dependency_overrides = {}

    # Assert
    assert response.status_code == 200
    assert response.json() == []
