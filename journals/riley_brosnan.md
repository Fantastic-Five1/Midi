## 11DEC2023

- Recreated README to include shields in for README and made "Issues" visible to the public
- Deleted console.log in searched.jsx
- Created Presentation for Demo

## 07DEC2023

- Debugged deployment issues with Album Detail page and hover effect on albums and artists. Found that public folder needed a "/" in the style sheet to fix broken picture issues

## 06DEC2023

- Debugged frontend with Cayman in order to pass pipeline
- Ran into issues with rendering context, so we had to separate out context files in order to fix pipeline

## 05DEC2023

- Created README
- Edited docs to include API, data_models, Integrations, and wire_frame
- Made tests for backend
  - Showed team what tests looked like and pair coded an additional test

## 04DEC2023

- Added artist_name to favorite_album NOT as a FK to favorite_artist because a customer can technically favorite an album by an artist while still not liking an artist.
- Fixed bug in average rating
- Cleaned up backend for unused variables/formatting to pass BLACK

## 01DEC2023

- Pair coded with Cayman to make app mobile ready in regards to CSS and other features to ensure that they showed up on both widths

## 30NOV2023

- Added comments section and comment modal (Cayman had a template from previous work).
- Need to add a way for the reviews to refresh when a new one is loaded since it is a modal
- Created photo feature on Album_Detail page with Dante

## 29NOV2023

- Built favorite albums page and worked with Dante to create delete function on both favorites page

- Worked with Cayman to rework Search Function to not break. Made it a little slower to help prevent errors

## 28NOV2023

- Built Search function with pictures but fails after 25 calls. Need to create a way to limit search calls or to slow down search
- Created Favorite_artist with images

## 27NOV2023

- Sick

## 22NOV2023

- Added POST to favorite_artist
- Added create account on home page
- Added search bar when token is present

## 21NOV2023

- Finished authentication on front end with vite. Had issues with getting
- VITE_APP_API_HOST to show localhost8000 do to a transfer from react to vite.
- Created modal for signup and connected signin and signup modal to backend.
- Applied search bar and signin/out based on whether a token is present.
- Need to add signin on signup modal and exit out feature.

## 20NOV2023

- Paired coding with Cayman and Dante to go over API calls and determine whether to use useContext or push it in the url with useLocation
- Fixed search results so that it returns a single artist when it only returns 1 result
- Added wikiArtist detail api feature to artist detail page

## 17NOV2023

- Finished CRUD of review
  - added average rating in backend calculated by PostgreSQL
- Should probably call this function immediately after every new review is created.
- Added Wiki blurbs on backend for front end to call.
  - Ran into an issue with album blurb because the json would push the space and/or the "+" to the end of the string after sending it to router
- FIXED (potentially) this issue by having front end concatenate string before sending it to backend

## 16NOV2023

- added CRUD with authenticator
  - favorite_artist
  - favorite_album
  - create for review
- altered DB for review to use username as a FK instead of id in order to only use 1 map function in FE for review
  - realized I could use the token to insert username or id into fastAPI endpoints without making the FE manually insert it
- potential stretch goal?
  - delete account
  - update account name
  - favorite artwork page

## 15NOV2023

- Added PUT function to favorite_artist
- Added Authenticate on backend for account
  - had issues with backend authenticate with token and hashed_password
  - left commented out get statement in end point account, in case it is needed for a get account info on frontend
- Had to make username unique
- Need to create a return statement for DuplicateAccountError in case a user tries to create an username that already exists

## 14NOV2023

- Presented wire-frame and databases
- troubleshooted wiki py doc so it can be used for a backend call for the info section.
- Also figured out a way to do a call for the album description

- Created fastapi calls for

  - POST account
  - POST favorite_artist
  - verified them in database using pg admin

- Had to change database remove "unique" for artist_mbid due to artist_mbid not being unique since it will be logged by multiple users

- Troubleshooted api calls with group (Cayman, E, Dante) for track and album art
- Had issues with having to add the same artist/album to different users.
  resulted in having to change FK only based on id. - Will cause duplicated data, but prevents having to save every artist/album.

## 13NOV2023

- forked and cloned skeleton and shared entries with teammates
- Added init.sql using 4 tables: 1. account 2. favorite_artist 3. favorite_album 4. review
  - will possibly need to add concerts if API call for concerts is not functional in the front end
- Worked with group to set up vite react (Cayman and Dante)
  this allowed to trouble shoot node_modules docker issues that were present in previous - "create react app"
- Added to docs file
- Uploaded and edited wire_frame (created by Cayman with input from group)
- Uploaded midi_data_models diagram
